// Assignment Operators

	// Basic Assignment Operator (=)
	// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable.

	let assignmentNumber = 8;
	console.log(assignmentNumber);

	// Addition Assignment Operator

	let totalNumber = assignmentNumber + 2;
	console.log('Result of addition assignment operator:' + totalNumber);

	// Arithmetic Operator (+, -, *, /, %)

	let x = 2;
	let y = 5;

	let sum = x + y;
	console.log("Result of addition operator: " + sum);

	let difference = x - y;
	console.log("Result of subtraction operator: " + difference);

	let product  = x * y;
	console.log("Result multiplication operator: " + product);

	let quotient  = x / y;
	console.log("Result of division operator: " + quotient);

	let remainder = y % x;
	console.log("Result of modulo operator: " + remainder);

	// Multiple Operators and Parentheses (MDAS and PEMDAS)

	/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/
                let mDas = 1 + 2 - 3 * 4 / 5;
                console.log("Result of mDas operation:" + mDas);

     // Comparison Operator

     // Equality Operator (==)

 //     console.log(1 == 1);
	// console.log(1 == 2);
	// console.log(1 == '1');
	// console.log(0 == false);
	// //Compares two strings that are the same 
	// console.log('juan' == 'juan');
	// //Compares a string with the variable "juan" declared above 
	// console.log('juan' == juan);

	// Logical Operators (&&, ||, !)

	let isLegalAge = true;
	let isRegistered = false;

	// Logical Operator (&& - Double Ampersand - And Operator)
	// Return true if all operands are true

	let allRequirementsMet = isLegalAge && isRegistered;
	console.log('Result of logical AND Operator ' + allRequirementsMet);

	// Logical Or Operator (|| - Double Pipe)
	// Returns true if one of the operands are true 

	// 1 || 1 = true
	// 1 || 0 = true
	// 0 || 1 = true
	// 0 || 0 = false

	let someRequirementsMet = isLegalAge || isRegistered; 
	console.log("Result of logical OR Operator: " + someRequirementsMet);

	// Logical Not Operator (! - Exclamation Point)
        // Returns the opposite value 
        let someRequirementsNotMet = !isRegistered;
        console.log("Result of logical NOT Operator: " + someRequirementsNotMet);